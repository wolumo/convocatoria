package examen.DAO;

import examen.POJO.Empleados;
import java.util.List;

public interface EmpleadoDAO {
    List<Empleados> ListarTodos();
    Empleados buscarPorID(int codigo);
    Empleados buscarPorApellido(String apellido);
    void registrarEmpleado(Empleados e);
    void actualizarEmpleado(int codigo);
    void eliminarEmpleado(int codigo);        
}
