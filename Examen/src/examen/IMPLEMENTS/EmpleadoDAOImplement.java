
package examen.IMPLEMENTS;

import examen.DAO.EmpleadoDAO;
import examen.POJO.Empleados;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmpleadoDAOImplement implements EmpleadoDAO {

    List<Empleados> lista = new ArrayList<>();

    @Override
    public List<Empleados> ListarTodos() {
        return lista;
    }

    @Override
    public Empleados buscarPorID(int codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public Empleados buscarPorApellido(String apellido) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registrarEmpleado(Empleados e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        lista.add(e);
    }

    @Override
    public void actualizarEmpleado(int codigo) {
        for (int e = 0; e < lista.size(); e++) {
            if (lista.get(e).getCodigo() == codigo) {
            System.out.println("Nombres");
            lista.get(e).setNombres(new Scanner(System.in).nextLine());
            System.out.println("Apellidos");
            lista.get(e).setApellidos(new Scanner(System.in).nextLine());
            System.out.println("Codigo (solo numeros)");
            lista.get(e).setCodigo(new Scanner(System.in).nextInt());
            System.out.println("Cedula");
            lista.get(e).setCedula(new Scanner(System.in).nextLine());
            System.out.println("Salario");
            lista.get(e).setSalario(new Scanner(System.in).nextFloat());
            System.out.println("Fecha de ingreso");
            lista.get(e).setFecha_ingreso(new Scanner(System.in).nextLine());
            }
        }
    }

    @Override
    public void eliminarEmpleado(int codigo) {
        for (int e = 0; e < lista.size(); e++) {
            if (lista.get(e).getCodigo() == codigo) {
                lista.remove(e);
            }
        }
    }

}
