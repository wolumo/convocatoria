package examen;

import examen.DAO.EmpleadoDAO;
import examen.IMPLEMENTS.EmpleadoDAOImplement;
import examen.POJO.Empleados;
import java.util.List;
import java.util.Scanner;

/*Crear una aplicación que guarde registros de
Empleado (Código, Cédula, nombres, apellidos, fecha_ingreso, salario) ,
calcule el salario neto e imprima la colilla por cada empleado(Código, Nombres, Apellidos, Salario bruto, antigüedad, Inss, IR, Salario Neto).
La antigüedad es el calculo de los años que lleva trabajando en la empresa.
Deberá crear una clase POJO para Empleado
Deberá crear DAO e implementaciones para que funcione con colecciones. (ya sea List, Set o Map)
La aplicación deberá agregar, editar, eliminar y buscar empleado (por código y apellidos)
No guardará en archivos ni secuenciales ni aleatorios.
Subir su proyecto al repositorio, no revisaré repositorios comprimidos.
Subir archivo .txt en el Eva con su nombre y la url de su repositorio del examen
 */
public class Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        EmpleadoDAO dao = new EmpleadoDAOImplement();
        int codigo = 0;
        String cedula = null;
        String nombre = null;
        String apellido = null;
        String fechaIn = null;
        float salario = 0;
        Empleados e;

        Scanner scan = new Scanner(System.in);
        int opc;
        boolean salir = false;

        while (!salir) {
            System.out.println();
            System.out.println("1. Agregar empleados");
            System.out.println("2. Eliminar empleados");
            System.out.println("3. Modificar empleados");
            System.out.println("4. Colilla empleados");
            System.out.println("5. Salir");
            System.out.println();
            opc = scan.nextInt();
            switch (opc) {
                case 1:
                    System.out.println("Nombres");
                    nombre = new Scanner(System.in).nextLine();
                    System.out.println("Apellidos");
                    apellido = new Scanner(System.in).nextLine();
                    System.out.println("Codigo (solo numeros)");
                    codigo = new Scanner(System.in).nextInt();
                    System.out.println("Cedula");
                    cedula = new Scanner(System.in).nextLine();
                    System.out.println("Salario");
                    salario = new Scanner(System.in).nextFloat();
                    System.out.println("Fecha de ingreso");
                    fechaIn = new Scanner(System.in).nextLine();
                    e = new Empleados(codigo, cedula, nombre, apellido, fechaIn, salario);
                    dao.registrarEmpleado(e);
                    break;
                case 2:
                    System.out.println();
                    System.out.println("Empleados actuales: ");
                    System.out.println();
                    dao.ListarTodos().forEach(x -> System.out.println(x.getCodigo() + " - " + x.getNombres() + "\n"));
                    System.out.println();
                    System.out.println("Eliminar por ID: ");
                    codigo = new Scanner(System.in).nextInt();
                    dao.eliminarEmpleado(codigo);

                    break;
                case 3:
                    System.out.println();
                    System.out.println("Empleados actuales: ");
                    dao.ListarTodos().forEach(x -> System.out.println(x.getCodigo() + " - " + x.getNombres() + "\n"));
                    System.out.println();
                    System.out.println("Modificar por ID: ");
                    codigo = new Scanner(System.in).nextInt();
                    dao.actualizarEmpleado(codigo);
                    break;
                case 4:
                    dao.ListarTodos().forEach(x -> System.out.println("Codigo: " + x.getCodigo() + "\nNombres: " + x.getNombres() + "\nApellidos: " + x.getApellidos() + "\nSalario bruto: " + x.getSalario() + "\nAntiguedad: " + x.getFecha_ingreso() + "\nINNS: 7.00%" + "\nIR: 15.00%\n" + "Salario neto: " + x.getSalario() * 0.22));
                    //dao.ListarTodos().size();
                    //System.out.println(dao.ListarTodos().size());

                    break;
                case 5:
                    salir = true;
                    break;
                default:
                    System.out.println("Opcion incorrecta");
            }
        }

    }

}
